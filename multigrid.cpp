/********************************************************************************
 **                                                                            **
 **  MultiGrid: A library with functions that can speed up iterative solvers   **
 **  Copyright (C) 2014 Gkratsia Tantilian                                     **
 **                                                                            **
 **  This file is part of MultiGrid.                                           **
 **                                                                            **
 **  MultiGrid is free software: you can redistribute it and/or modify         **
 **  it under the terms of the GNU Lesser General Public License as            **
 **  published by the Free Software Foundation, either version 3 of the        **
 **  License, or (at your option) any later version.                           **
 **                                                                            **
 **  MultiGrid is distributed in the hope that it will be useful,              **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
 **  GNU Lesser General Public License for more details.                       **
 **                                                                            **
 **  You should have received a copy of the GNU Lesser General Public License  **
 **  along with MultiGrid.  If not, see <http://www.gnu.org/licenses/>.        **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 **                                                                            **
 **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
 **          University: Aristotle University of Thessaloniki                  **
 **                Date: September 2014                                        **
 **             Version: Alpha 1.0                                             **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 */

/**
 * \file multigrid.cpp
 * \brief This file contains functions needed for *multigrid* (relaxation/prolongation...) of a 2D and 3D domain.
 */

/* INCLUDES */
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <algorithm>
#include "multigrid.hpp"
/* INCLUDES */

/* NAMESPACES */
using namespace std;
/* NAMESPACES */


/* 2D Functions */
/* -------------------------------------------------------------------------------------------------------- */
int MGRestrict2D(vector< vector<double> > src, vector< vector<double> > &trgt)
    /*
     * \function MGRestrict2D
     * \brief Restriction function for 2D residual
     *      1  | 1   2   1 |
     *     --- | 2   4   2 |
     *     16  | 1   2   1 |
     * \param src A 2D vector with the initial (source) residual
     * \param trgt A 2D vector with the final (target) residual
     */
{
    if ((src.size() - 1)%2 != 0 || (src[0].size() - 1)%2 != 0) return 1;

    vector< vector<double> > residual((src.size() - 1)/2 + 1, vector<double>((src[0].size() - 1)/2 + 1,0));
    int i_f, j_f;

    for (unsigned int j = 0; j < residual[0].size(); ++j) {
        for (unsigned int i = 0; i < residual.size(); ++i) {
            i_f = 2*i;
            j_f = 2*j;
            if (i == 0 || j == 0 || i == (residual.size() - 1) || j == (residual[0].size() - 1) )  {
                residual[i][j] = src[i_f][j_f];
            } else {
                residual[i][j] = (src[i_f - 1][j_f - 1] + 2.*src[i_f][j_f - 1]  +    src[i_f + 1][j_f - 1] +
                               2.*src[i_f - 1][j_f]     + 4.*src[i_f][j_f]      + 2.*src[i_f + 1][j_f] +
                                  src[i_f - 1][j_f + 1] + 2.*src[i_f][j_f + 1]  +    src[i_f + 1][j_f + 1])/16.0;
            }
        }
    }

    trgt = residual;
    return 0;
} /* MGRestrict2D */
/* -------------------------------------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------------------------------------- */
int MGProlongate2D(vector< vector<double> > src, vector< vector<double> > &trgt)
    /*
     * \function MGProlongate2D
     * \brief Prolongation function for 2D residual
     *      1  | 1   2   1 |
     *     --- | 2   4   2 |
     *      4  | 1   2   1 |
     * \param src A 2D vector with the initial (source) residual
     * \param trgt A 2D vector with the final (target) residual
     */
{
    int i_c, j_c;

    for (unsigned int j = 1; j < trgt[0].size() - 1; ++j) {
        for (unsigned int i = 1; i < trgt.size() - 1; ++i) {
            i_c = i/2;
            j_c = j/2;
            if(i%2 == 0 && j%2 == 0) {
                trgt[i][j] = src[i_c][j_c] + trgt[i][j];
            } else if (i%2 == 0 && j%2 != 0) {
                trgt[i][j] = (src[i_c][j_c] + src[i_c][j_c + 1])/2.0 + trgt[i][j];
            } else if (i%2 != 0 && j%2 == 0) {
                trgt[i][j] = (src[i_c][j_c] + src[i_c + 1][j_c])/2.0 + trgt[i][j];
            } else if (i%2 != 0 && j%2 != 0) {
                trgt[i][j] = (src[i_c][j_c] + src[i_c + 1][j_c] + src[i_c][j_c + 1] + src[i_c + 1][j_c + 1])/4.0
                    + trgt[i][j];
            }
        }
    }

    return 0;
} /* MGProlongate2D */
/* -------------------------------------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------------------------------------- */
double JRelaxation2D(vector< vector<double> > &v, vector< vector<double> > f, double d, vector< vector<double> > &r)
    /*
     * \function JRelaxation2D
     * \brief Jacobean relaxation
     * \param v A 2D vector with the numerical solution
     * \param trgt A 2D vector with the source function (must be in same dimensions with v)
     * \param d A double number that represents the discretization step
     * \param r A 2D vector which represents the residual (to be calculated).
     * \return -2: if the vector dimensions don't match, -1 if residual was not calculated, the norm(residual) otherwise
     */
{
    double ret_val = -1.;
    if (v.size() != f.size() || v[0].size() != f[0].size()) {
        return -2.;
    }

    for (unsigned int i = 1; i < v.size() - 1; ++i) {
        for (unsigned int j = 1; j < v[0].size() - 1; ++j) {
            v[i][j] = (v[i-1][j] + v[i+1][j] + v[i][j-1] + v[i][j+1])/4.0 - d*d*f[i][j]/4.0;
        }
    }

    if (r.size() == v.size() && r.size() == f.size()) {
        vector<double> residual;
        for (unsigned int i = 1; i < v.size() - 1; ++i) {
            for (unsigned int j = 1; j < v[0].size() - 1; ++j) {
                r[i][j] = f[i][j] - ((v[i-1][j] + v[i+1][j] + v[i][j-1] + v[i][j+1] - 4.0*v[i][j])/(d*d));
                residual.push_back(fabs(r[i][j]));
            }
        }
        ret_val = *max_element(residual.begin(), residual.end());
    }
    return ret_val;
} /* JRelaxation2D */
/* -------------------------------------------------------------------------------------------------------- */




/* 3D Functions */
/* -------------------------------------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------------------------------------- */
