## ********************************************************************************
## **                                                                            **
## **  MultiGrid: A library with functions that can speed up iterative solvers   **
## **  Copyright (C) 2014 Gkratsia Tantilian                                     **
## **                                                                            **
## **  This file is part of MultiGrid.                                           **
## **                                                                            **
## **  MultiGrid is free software: you can redistribute it and/or modify         **
## **  it under the terms of the GNU Lesser General Public License as            **
## **  published by the Free Software Foundation, either version 3 of the        **
## **  License, or (at your option) any later version.                           **
## **                                                                            **
## **  MultiGrid is distributed in the hope that it will be useful,              **
## **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
## **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
## **  GNU Lesser General Public License for more details.                       **
## **                                                                            **
## **  You should have received a copy of the GNU Lesser General Public License  **
## **  along with MultiGrid.  If not, see <http://www.gnu.org/licenses/>.        **
## **                                                                            **
## **  ------------------------------------------------------------------------  **
## **                                                                            **
## **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
## **          University: Aristotle University of Thessaloniki                  **
## **                Date: September 2014                                        **
## **             Version: Alpha 1.0                                             **
## **                                                                            **
## **  ------------------------------------------------------------------------  **


QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TEMPLATE = app
TARGET = MultiGrid
DEPENDPATH += .
INCLUDEPATH += qcustomplot
OBJECTS_DIR = ./objects/

QMAKE_CXXFLAGS += -Wno-deprecated -Wno-unused
CONFIG += silent

# Input
HEADERS += plot/mainwindow.h \
            multigrid.hpp \
            plot/qcustomplot/qcustomplot.h
SOURCES += main.cpp \
            plot/mainwindow.cpp \
            multigrid.cpp \
            plot/qcustomplot/qcustomplot.cpp

FORMS    += plot/mainwindow.ui
