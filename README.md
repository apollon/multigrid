[TOC]

Introduction to multigrid
=========================

![MG3D](https://bitbucket.org/repo/EAGLE4/images/3936541575-logo_transparent.png "MultiGrid")

Introduction
------------

Multigrid methods in numerical analysis are a group of algorithms for solving differential equations using a 
hierarchy of discretizations.

The main idea of multigrid is to accelerate the convergence of a basic iterative method by global correction 
from time to time, accomplished by solving a coarse problem. This principle is similar to interpolation between 
coarser and finer grids. The typical application for multigrid is in the numerical solution of elliptic partial 
differential equations in two or more dimensions.

Theoretical explanation
-----------------------

Lets consider a system of linear equations denoted by: 

A _u_ = _f_ (eq.1)

If _u_ is the exact solution of the problem and _v_ is the numerical approximation to that solution, one can
say that these two functions are related with the following relation:

_u_ = _v_ + **e** => **e** = _u_ - _v_ (eq.2)

Where **e** represents the **error** of our solution. This error is a vector. Its magnitude may be measured by
any of the standard vector norms. We choose the maximum (or infinity) norm:

||**e**||_∞ = max|e_i|

Although the error itself is unknown as the solution, a computable measure of how _v_ approximates _u_ is the 
**residual**:

**r** = _f_ - A _v_ (from eq.1) (eq.3)

Now, if we replace _v_ in (eq.3) from the (eq.2), we get:

**r** = f - A(_u_ - **e**) => **r** = f - A _u_ + A **e**

But according to (eq.1) A _u_ = f so we get:

**A e = r**

Which means that if we solve the same system by have the residual as the _source function_, we actually solve 
for the **error**.

* If we have a low frequency error in a high resolution grid, then the error eliminates slower
* By reducing the domain resolution, low frequency errors appear as high frequency errors, which means they can
be eliminated faster.

That is the reason we choose **Multigrid** methods.

The algorithm
-------------

In multigrid algorithm there are these terms:

* **Relaxation (or Smoothing)**: Reducing high frequency errors. For example using a few iterations of Gauss-Seidel or Jacobi method.
* **Restriction**: Downsampling the **residual** to a coarser level / grid.
* **Prolongation (or Interpolation)**: Interpolating a correction computed on a coarser grid into a finer grid.

For an elliptic, Poisson-like equations, the steps of the algorithm are the following:

* A few _Relaxations_
* _Restriction_
* A few _Relaxations_
* ... Repeat until we reach a 3x3(x3) grid
* Repeat _Prolongations_ until we reach the finest level / grid, where we are solving for _v_

These steps describe a full V-cycle. When we reach to the finest level we have corrected the solution by 
eliminating low frequency errors. If we didn’t reach the desired accuracy, we may repeat the V-cycle.
We could also use the so called W-cycle, where we don’t reach the coarsest level at once.

The reason why it is a huge speed up is that the coarser grids are in a much lower resolution related to the finest grid,
which means less iterations.

What is included
----------------

**UNDER DEVELPMENT: NOT FULLY FUNCTIONAL**

This library is part of a bigger project, which will be available as soon as it is completed. It is implemented in 
C/C++, openMP and CUDA. For plotting the results we use the [QCustomPlot](http://www.qcustomplot.com/ "QCustomPlot") library.

The following functions are available:

```
#!c++
double JRelaxation2D(vector< vector<double> > &v, vector< vector<double> > f, double d, vector< vector<double> > &r)
```

* **JRelaxation2D**: A simple implementation of Jacobi method for a 2D Poisson equation. 
    Parameters:
    * **v**: A 2D vector that represents the numerical solution.
    * **f**: A 2D vector that represents the source function.
    * **d**: Assuming that the grids are square, d represents the domain discretization spacing.
    * **r**: A 2D vector that represents the residual.
    * **return**: Returns a double which is the maximum value of the residual (||r||_∞).
