#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QDesktopWidget>
#include <QScreen>
#include <QMessageBox>
#include <QMetaEnum>
#include <algorithm>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setGeometry(400, 250, 542, 390);
    set_x_plot_data(QVector<double>(1,1));
    set_y_plot_data(QVector<double>(1,1));
}

void MainWindow::callPlotFunction(int functionIndex)
{
    switch (functionIndex)
    {
        case 0: plotData2D(ui->customPlot); break;
        case 1: plotData2DLog(ui->customPlot); break;
    }
    setWindowTitle("MultiGrid Results: "+plotTitle);
    statusBar()->clearMessage();
    ui->customPlot->replot();
}

void MainWindow::plotData2D(QCustomPlot *customPlot)
{
    double xmin, xmax, ymin, ymax;
    xmin = *std::min_element(x_vec.begin(), x_vec.end());
    xmax = *std::max_element(x_vec.begin(), x_vec.end());
    ymin = *std::min_element(y_vec.begin(), y_vec.end());
    ymax = *std::max_element(y_vec.begin(), y_vec.end());
    plotTitle = "Plot 2D";
    // create graph and assign data to it:
    customPlot->addGraph();
    customPlot->graph(0)->setData(x_vec, y_vec);
    // give the axes some labels:
    customPlot->xAxis->setLabel("x");
    customPlot->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    customPlot->xAxis->setRange(xmin, xmax);
    customPlot->yAxis->setRange(ymin, ymax);
    // make range draggable and zoomable:
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
}

void MainWindow::plotData2DLog(QCustomPlot *customPlot)
{
    double xmin, xmax, ymin, ymax;
    xmin = *std::min_element(x_vec.begin(), x_vec.end());
    xmax = *std::max_element(x_vec.begin(), x_vec.end());
    ymin = *std::min_element(y_vec.begin(), y_vec.end());
    ymax = *std::max_element(y_vec.begin(), y_vec.end());

    plotTitle = "Plot 2D Logarithmic";
    customPlot->setNoAntialiasingOnDrag(true);
    customPlot->addGraph();
    customPlot->graph(0)->setData(x_vec, y_vec);

    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
    customPlot->yAxis->setScaleLogBase(100);
    customPlot->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    customPlot->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    customPlot->yAxis->setSubTickCount(10);
    // give the axes some labels:
    customPlot->xAxis->setLabel("x");
    customPlot->yAxis->setLabel("log(y)");
    // set axes ranges, so we see all data:
    customPlot->xAxis->setRange(xmin, xmax);
    customPlot->yAxis->setRange(ymin, ymax);
    // make range draggable and zoomable:
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    // connect signals so top and right axes move in sync with bottom and left axes:
    connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
}

MainWindow::~MainWindow()
{
    delete ui;
}
