/********************************************************************************
 **                                                                            **
 **  MultiGrid: A library with functions that can speed up iterative solvers   **
 **  Copyright (C) 2014 Gkratsia Tantilian                                     **
 **                                                                            **
 **  This file is part of MultiGrid.                                           **
 **                                                                            **
 **  MultiGrid is free software: you can redistribute it and/or modify         **
 **  it under the terms of the GNU Lesser General Public License as            **
 **  published by the Free Software Foundation, either version 3 of the        **
 **  License, or (at your option) any later version.                           **
 **                                                                            **
 **  MultiGrid is distributed in the hope that it will be useful,              **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
 **  GNU Lesser General Public License for more details.                       **
 **                                                                            **
 **  You should have received a copy of the GNU Lesser General Public License  **
 **  along with MultiGrid.  If not, see <http://www.gnu.org/licenses/>.        **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 **                                                                            **
 **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
 **          University: Aristotle University of Thessaloniki                  **
 **                Date: September 2014                                        **
 **             Version: Alpha 1.0                                             **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "qcustomplot/qcustomplot.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

        void callPlotFunction(int functionIndex);
        void plotData2D(QCustomPlot *customPlot);
        void plotData2DLog(QCustomPlot *customPlot);

        void setupPlayground(QCustomPlot *customPlot);

        void set_x_plot_data(QVector<double> x){ x_vec = x;}
        void set_y_plot_data(QVector<double> y){ y_vec = y;}
        void set_title(QString title) { plotTitle = title; }

        QVector<double>   x_vec;
        QVector<double>   y_vec;

    private:
        Ui::MainWindow *ui;
        QString plotTitle;
};

#endif // MAINWINDOW_H
