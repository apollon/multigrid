/********************************************************************************
 **                                                                            **
 **  MultiGrid: A library with functions that can speed up iterative solvers   **
 **  Copyright (C) 2014 Gkratsia Tantilian                                     **
 **                                                                            **
 **  This file is part of MultiGrid.                                           **
 **                                                                            **
 **  MultiGrid is free software: you can redistribute it and/or modify         **
 **  it under the terms of the GNU Lesser General Public License as            **
 **  published by the Free Software Foundation, either version 3 of the        **
 **  License, or (at your option) any later version.                           **
 **                                                                            **
 **  MultiGrid is distributed in the hope that it will be useful,              **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
 **  GNU Lesser General Public License for more details.                       **
 **                                                                            **
 **  You should have received a copy of the GNU Lesser General Public License  **
 **  along with MultiGrid.  If not, see <http://www.gnu.org/licenses/>.        **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 **                                                                            **
 **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
 **          University: Aristotle University of Thessaloniki                  **
 **                Date: September 2014                                        **
 **             Version: Alpha 1.0                                             **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 */

#include <QApplication>
#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <iomanip>
#include <cstdlib>
#include <algorithm>
#include "plot/mainwindow.h"
#include "multigrid.hpp"

using namespace std;

static int multigrid_levels(int x_size, int y_size)
{
   return min(log10(x_size - 1)/log10(2), log10(y_size - 1)/log10(2));
}

static int init_circular_source(vector< vector<double> > &f, vector <double> bounds, double r = 1., double xc = 0., double yc = 0.)
{
    double  xmin = bounds[0], xmax = bounds[1], ymin = bounds[2], ymax = bounds[3];
    double  dx = (xmax - xmin)/f.size();
    double  dy = (ymax - ymin)/f[0].size();
    double  x, y, ri;

    for (unsigned int i = 0; i < f.size(); ++i) {
        for (unsigned int j = 0; j < f[0].size(); ++j) {
            x = xmin + i*dx; y = ymin + j*dy;
            ri = sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc));
            if (ri <= r) {
                f[i][j] = -1/(ri*ri);
            }
        }
    }
    return 0;
}

static int output_1D_vector(vector<double> f, const char *filename)
{
    int i = 0;
    fstream file;
    file.open(filename, ios::out);
    for (vector<double>::iterator row = f.begin(); row != f.end(); ++row) {
        file << i << "\t" << fixed << setprecision(8) << *row << endl;
        i++;
    }
    file.close();
    return 0;
}

static int output_2D_vector(vector< vector<double> > f, const char *filename)
{
    int i = 0, j = 0;
    fstream file;
    file.open(filename, ios::out);
    for (vector< vector<double> >::iterator row = f.begin(); row != f.end(); ++row) {
        for (vector<double>::iterator col = row->begin(); col != row->end(); ++ col) {
            file << i << "\t" << j << "\t" << fixed << setprecision(8) << *col << endl;
            i++;
        }
        i = 0;
        j++;
    }
    file.close();
    return 0;
}

static int output_2D_vector_grid(vector< vector<double> > f, const char *filename)
{
    int i = 0, j = 0;
    fstream file;
    file.open(filename, ios::out);
    for (vector< vector<double> >::iterator row = f.begin(); row != f.end(); ++row) {
        for (vector<double>::iterator col = row->begin(); col != row->end(); ++ col) {
            file << fixed << setprecision(8) << *col << "\t";
            i++;
        }
        i = 0;
        j++;
        file << endl;
    }
    file.close();
    return 0;
}

static int initialise_residual(vector< vector< vector<double> > > &res, int dx, int dy, unsigned int levels)
{
    int d_x = dx, d_y = dy;
    for (unsigned int i = 0; i < levels; ++i) {
        res.push_back(vector< vector<double> >(d_x, vector<double>(d_y, 0)));
        d_x = (d_x - 1)/2 + 1;
        d_y = (d_y - 1)/2 + 1;
    }
    return 0;
}

static int initialise_solution(vector< vector< vector<double> > > &v, int dx, int dy, unsigned int levels)
{
    int d_x = dx, d_y = dy;
    for (unsigned int i = 0; i < levels; ++i) {
        v.push_back(vector< vector<double> >(d_x, vector<double>(d_y, 1)));
        d_x = (d_x - 1)/2 + 1;
        d_y = (d_y - 1)/2 + 1;
    }
    return 0;
}

static int initialise_source_levels(vector< vector< vector<double> > > &f, int dx, int dy, unsigned int levels)
{
    int d_x = dx, d_y = dy;
    for (unsigned int i = 0; i < levels; ++i) {
        f.push_back(vector< vector<double> >(d_x, vector<double>(d_y, 0)));
        d_x = (d_x - 1)/2 + 1;
        d_y = (d_y - 1)/2 + 1;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    /* STEP 1: Define and initialise the domain and the functions. */
    int     GRID_SIZE   = 65;
    int     V_CYCLES    = 10;
    vector< vector< vector<double> > > v;
    vector< vector< vector<double> > > f;
    vector< vector< vector<double> > > residual_vec;
    vector<double> bounds;
    vector<double> r_norm;
    double xmin, ymin, xmax, ymax, d;
    int levels = multigrid_levels(GRID_SIZE, GRID_SIZE);

    xmin = ymin = -5.;
    xmax = ymax =  5.;
    d = (ymax - ymin)/GRID_SIZE;

    bounds.push_back(xmin); // xmin
    bounds.push_back(xmax); // xmax
    bounds.push_back(ymin); // ymin
    bounds.push_back(ymax); // ymax

    /* Initialise the residual/source/solution vector */
    initialise_residual(residual_vec, GRID_SIZE, GRID_SIZE, levels);
    initialise_solution(v, GRID_SIZE, GRID_SIZE, levels);
    initialise_source_levels(f, GRID_SIZE, GRID_SIZE, levels);

    /* STEP 2: define (initialise) the source function. */
    init_circular_source(f[0], bounds, 5);

    for (int cycles = 0; cycles < V_CYCLES; ++cycles) {
        /* STEP 3: The V-cycle (ascending) */
        /* STEP 3.1: First few relaxations to eliminate low frequency errors. */
        for (int i = 0; i < 40; i++) r_norm.push_back(JRelaxation2D(v[0], f[0], d, residual_vec[0]));
        /* STEP 3.2: Restriction. Go to coarser levels, with a few relaxations each time. */
        for (int i = 1; i < levels; ++i) {
            MGRestrict2D(residual_vec[i - 1], f[i]);
            for (int j = 0; j < 4; ++j)
                JRelaxation2D(v[i], f[i], (xmax - xmin)/(residual_vec[i].size()), residual_vec[i]);
        }

        /* STEP 4: The V-cycle (descending) */
        /* STEP 4.1: Prolongation. Go to the finer levels by correcting the errors. Reach the finest level. */
        /*
        for (int i = levels - 2; i >= 0; --i) {
            MGProlongate2D(v[i + 1], v[i]);
            for (int j = 0; j < 4; ++j)
                JRelaxation2D(v[i], f[i], (xmax - xmin)/(residual_vec[i].size()), residual_vec[i]);
        }
        */

        /* STEP 4.2: Final few relaxations to eliminate low frequency errors. */
        for (int i = 0; i < 4; i++) r_norm.push_back(JRelaxation2D(v[0], f[0], d, residual_vec[0]));
    }

#if 0
    char filename[80];
    for (int i = 0; i < levels; ++i) {
        sprintf(filename, "output/sol_%i.dat", i);
        output_2D_vector(v[i], filename);
    }
    /* STEP 5: Output the results. */
    output_2D_vector(v, "output/solution.dat");
#endif

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QApplication::setGraphicsSystem("raster");
#endif
    QApplication a(argc, argv);
    MainWindow w, w1;
    QVector<double>x(r_norm.size());
    QVector<double>y(r_norm.size());
    for (unsigned int i=0; i<r_norm.size(); ++i)
    {
        x[i] = i;
        y[i] = r_norm[i];
    }
    w.set_x_plot_data(x);
    w.set_y_plot_data(y);
    w.callPlotFunction(1);
    w.setWindowTitle("Residual");
    w.show();

    return a.exec();
}
