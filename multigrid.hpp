/********************************************************************************
 **                                                                            **
 **  MultiGrid: A library with functions that can speed up iterative solvers   **
 **  Copyright (C) 2014 Gkratsia Tantilian                                     **
 **                                                                            **
 **  This file is part of MultiGrid.                                           **
 **                                                                            **
 **  MultiGrid is free software: you can redistribute it and/or modify         **
 **  it under the terms of the GNU Lesser General Public License as            **
 **  published by the Free Software Foundation, either version 3 of the        **
 **  License, or (at your option) any later version.                           **
 **                                                                            **
 **  MultiGrid is distributed in the hope that it will be useful,              **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
 **  GNU Lesser General Public License for more details.                       **
 **                                                                            **
 **  You should have received a copy of the GNU Lesser General Public License  **
 **  along with MultiGrid.  If not, see <http://www.gnu.org/licenses/>.        **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 **                                                                            **
 **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
 **          University: Aristotle University of Thessaloniki                  **
 **                Date: September 2014                                        **
 **             Version: Alpha 1.0                                             **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 */

#ifndef MULTIGRID_HPP
#define MULTIGRID_HPP
/* INCLUDES */
#include <vector>
/* INCLUDES */

/* CLASSES */
/* CLASSES */

/* FUNCTIONS */
int MGRestrict2D(std::vector< std::vector<double> > src, std::vector< std::vector<double> > &trgt);
int MGProlongate2D(std::vector< std::vector<double> > src, std::vector< std::vector<double> > &trgt);
double JRelaxation2D(std::vector< std::vector<double> > &v, std::vector< std::vector<double> > f, double d, std::vector< std::vector<double> > &r);
/* FUNCTIONS */
#endif
